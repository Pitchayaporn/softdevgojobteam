/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package pos;
import component.BuyProductable;
import component.ProductListPanel;

import dao.RecieptDetailDao;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;

import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import model.Member;
import model.Product;
import model.Reciept;
import model.RecieptDetail;
import service.EmployeeService;
import service.MemberService;
import service.ProductService;
import service.PromotionService;
import service.RecieptService;
import ui.MemberDialog;
import ui.PosRecieptDialog;
import ui.SearchMemDialog;


/**
 *
 * @author nutta
 */
public class PosPanel extends javax.swing.JPanel implements BuyProductable {

    ArrayList<Product> products;
    ProductService productService = new ProductService();
    RecieptService recieptService = new RecieptService();
    Reciept reciept;
    private final ProductListPanel productListPanel;
    private Component rootPane;
    EmployeeService employeeService = new EmployeeService();
    MemberService memberService = new MemberService();
    private RecieptDetailDao editedTable;
    private List<Reciept> list;
    private Member M = new Member();
    private static float totaldiscount = 0;
    private static float  totalprice = 0;
    private static float  lastprice;

    public static float getLastprice() {
        return lastprice;
    }

    public static void setLastprice(float lastprice) {
        PosPanel.lastprice = lastprice;
    }

    public static float getTotaldiscount() {
        return totaldiscount;
    }

    public static void setTotaldiscount(float totaldiscount) {
        PosPanel.totaldiscount = totaldiscount;
    }

   

   
    

    

    /**
     * Creates new form posPanel
     */
    public PosPanel() {
        initComponents();
        reciept = new Reciept();
//        reciept.setEmployee(EmployeeService.getCurrentUser());

        tblRecieptDetail.setModel(new AbstractTableModel() {
            String[] headers = {"Name", "Price", "Qty", "Type", "Sweet", "TotalPrice"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                if (reciept != null && reciept.getRecieptDetails() != null) {
                    return reciept.getRecieptDetails().size();
                } else {
                    return 0;
                }
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                if (reciept != null && reciept.getRecieptDetails() != null) {
                    ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                    if (rowIndex < recieptDetails.size()) {
                        RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                        switch (columnIndex) {
                            case 0:
                                return recieptDetail.getProductName();
                            case 1:
                                return recieptDetail.getProductPrice();
                            case 2:
                                return recieptDetail.getQty();
                            case 3:
                                return recieptDetail.getProductType();
                            case 4:
                                return recieptDetail.getProductSweet();
                            case 5:
                                return recieptDetail.getTotalPrice();
                            default:
                                throw new AssertionError();
                        }
                    }
                }
                return null;
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                if (reciept != null && reciept.getRecieptDetails() != null) {
                    ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                    if (rowIndex < recieptDetails.size()) {
                        RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                        if (columnIndex == 2) {
                            try {
                                int qty = Integer.parseInt((String) aValue);
                                if (qty < 1) {
                                    return;
                                }
                                recieptDetail.setQty(qty);
                                reciept.calculateTotal();
                                refreshReciept();
                            } catch (NumberFormatException e) {
                                // Handle the NumberFormatException appropriately
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return columnIndex == 2 && reciept != null && reciept.getRecieptDetails() != null && rowIndex < reciept.getRecieptDetails().size();
            }
        });

        productListPanel = new ProductListPanel();
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        scrProductList = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblRecieptDetail = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        btnConfirm = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        asdasda = new javax.swing.JLabel();
        txSubtotal = new javax.swing.JLabel();
        txTotal = new javax.swing.JLabel();
        txsubtotal = new javax.swing.JLabel();
        txDiscount = new javax.swing.JLabel();
        txdoasdas = new javax.swing.JLabel();
        asd = new javax.swing.JPanel();
        lablepromoname = new javax.swing.JLabel();
        btnMember = new javax.swing.JButton();
        txpromoname = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txPointdiscount = new javax.swing.JLabel();
        btnMemberAdd = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(1050, 565));

        jPanel2.setBackground(new java.awt.Color(200, 178, 134));

        scrProductList.setBackground(new java.awt.Color(200, 178, 134));
        scrProductList.setForeground(new java.awt.Color(255, 255, 255));
        scrProductList.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        tblRecieptDetail.setBackground(new java.awt.Color(255, 255, 255));
        tblRecieptDetail.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblRecieptDetail.setForeground(new java.awt.Color(0, 0, 0));
        tblRecieptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblRecieptDetail);

        jPanel4.setBackground(new java.awt.Color(180, 143, 81));
        jPanel4.setForeground(new java.awt.Color(153, 153, 153));

        btnConfirm.setBackground(new java.awt.Color(153, 255, 153));
        btnConfirm.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnConfirm.setText("Confirm");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(255, 102, 102));
        btnCancel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnCancel.setText("Reset");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        asdasda.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        asdasda.setForeground(new java.awt.Color(0, 0, 0));
        asdasda.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        asdasda.setText("Total");

        txSubtotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txSubtotal.setForeground(new java.awt.Color(0, 0, 0));
        txSubtotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txSubtotal.setText("0.0  ");

        txTotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txTotal.setForeground(new java.awt.Color(0, 0, 0));
        txTotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txTotal.setText("0.0  ");

        txsubtotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txsubtotal.setForeground(new java.awt.Color(0, 0, 0));
        txsubtotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txsubtotal.setText("Subtotal");

        txDiscount.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txDiscount.setForeground(new java.awt.Color(0, 0, 0));
        txDiscount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txDiscount.setText("0.0  ");

        txdoasdas.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txdoasdas.setForeground(new java.awt.Color(0, 0, 0));
        txdoasdas.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txdoasdas.setText("Discount");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txsubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txdoasdas, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txSubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(84, 84, 84))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(asdasda, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(82, 82, 82)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(txSubtotal)
                        .addGap(15, 15, 15))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(txsubtotal)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txDiscount)
                    .addComponent(txdoasdas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txTotal)
                    .addComponent(asdasda, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(12, 12, 12)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );

        asd.setBackground(new java.awt.Color(176, 124, 72));

        lablepromoname.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lablepromoname.setForeground(new java.awt.Color(0, 0, 0));
        lablepromoname.setText("Promotion name : -");

        btnMember.setBackground(new java.awt.Color(255, 153, 0));
        btnMember.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnMember.setForeground(new java.awt.Color(255, 255, 255));
        btnMember.setText("Member");
        btnMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMemberActionPerformed(evt);
            }
        });

        txpromoname.setBackground(new java.awt.Color(255, 153, 0));
        txpromoname.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txpromoname.setForeground(new java.awt.Color(255, 255, 255));
        txpromoname.setText("Promotions");
        txpromoname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txpromonameActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Add");

        txPointdiscount.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txPointdiscount.setForeground(new java.awt.Color(0, 0, 0));
        txPointdiscount.setText("Point Discount : -");

        btnMemberAdd.setBackground(new java.awt.Color(255, 153, 0));
        btnMemberAdd.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnMemberAdd.setForeground(new java.awt.Color(255, 255, 255));
        btnMemberAdd.setText("Member Add");
        btnMemberAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMemberAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout asdLayout = new javax.swing.GroupLayout(asd);
        asd.setLayout(asdLayout);
        asdLayout.setHorizontalGroup(
            asdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(asdLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addGap(38, 38, 38)
                .addGroup(asdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnMember, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txPointdiscount))
                .addGap(27, 27, 27)
                .addComponent(btnMemberAdd)
                .addGap(35, 35, 35)
                .addGroup(asdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txpromoname)
                    .addComponent(lablepromoname))
                .addGap(45, 45, 45))
        );
        asdLayout.setVerticalGroup(
            asdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(asdLayout.createSequentialGroup()
                .addGroup(asdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(asdLayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jLabel2))
                    .addGroup(asdLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(asdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnMember, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnMemberAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txpromoname, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(asdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lablepromoname)
                            .addComponent(txPointdiscount))))
                .addContainerGap(65, Short.MAX_VALUE))
        );

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Point Of Sell");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 868, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jLabel1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(asd, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(asd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(scrProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 552, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        clearReciept();
        refreshReciept();
        System.out.println("Clear Table");
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        Member editedMember = new Member();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        Poscash poscash = new Poscash(M);
        poscash.setLocationRelativeTo(this);
        poscash.setVisible(true);
        poscash.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e) {
                reciept.setCash(RecieptService.getCurrentreciept().getCash());
               MemberService memService = new MemberService();
               //set member to receipt
               if(MemberService.getCurrentMember()!= null){
              reciept.setMemId(MemberService.getCurrentMember().getId()); 
               }
              reciept.setEmpId(EmployeeService.getCurrentUser().getId());
                System.out.println("this current Promotion = "+PromotionService.getCurrentPromotion());
              if(PromotionService.getCurrentPromotion()!=null){
                   reciept.setPmtId(PromotionService.getCurrentPromotion().getId());
                     System.out.println("this receipt promotion id is "+PromotionService.getCurrentPromotion().getId());
              }else if(PromotionService.getCurrentPromotion()==null){
                reciept.setPmtId(0);
              }
              RecieptService.setCurrentreciept(reciept);
               
                System.out.println("before add receipt =" +reciept);
                recieptService.addNew(reciept);
                

                
                System.out.println("" + reciept);
                openRecieptDialog();
                //clear discount
                totaldiscount = 0;
                clearReciept();
                txDiscount.setText("0.0");
               
                Member memcurr = memService.currentMember = new Member();
                System.out.println(memcurr);
                System.out.println("Add into RecieptDetail");
                
                System.out.println("after reset receipt and add to current=" +reciept);
            }
            });
        
    }//GEN-LAST:event_btnConfirmActionPerformed

    private void btnMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMemberActionPerformed
        Member editedMember = new Member();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PosMemberDialog posMemberDialog = new PosMemberDialog();
        posMemberDialog.setLocationRelativeTo(this);
        posMemberDialog.setVisible(true);
         posMemberDialog.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e) {
                int memdiscount = posMemberDialog.getMemdiscount();
                totaldiscount = totaldiscount + memdiscount;
                txDiscount.setText("" + totaldiscount);
                txPointdiscount.setText("Point Discount : " + memdiscount);
                lastprice =reciept.getTotal()-totaldiscount;
                txTotal.setText(""+lastprice);
                
                
            }
        });
    }//GEN-LAST:event_btnMemberActionPerformed

    private void txpromonameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txpromonameActionPerformed
                Member editedMember = new Member();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PosPromotionDialog posPromotionDialog = new PosPromotionDialog(frame,reciept );
        posPromotionDialog.setLocationRelativeTo(this);
        posPromotionDialog.setVisible(true);
                 posPromotionDialog.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e) {
                String promoname = posPromotionDialog.getEditedPromotion().getName();
                lablepromoname.setText("Promotion name : " + promoname);
                totaldiscount = totaldiscount + posPromotionDialog.getPromodiscount();
                 txDiscount.setText("" + totaldiscount);
                 lastprice = reciept.getTotal()-totaldiscount;
                        txTotal.setText(""+lastprice);

            }
        });
    }//GEN-LAST:event_txpromonameActionPerformed

    private void btnMemberAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMemberAddActionPerformed
         Member editedMember = new Member();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        MemberDialog userDialog = new MemberDialog(frame, editedMember);
        userDialog.setLocationRelativeTo(this);
        userDialog.setVisible(true);
    }//GEN-LAST:event_btnMemberAddActionPerformed

    public void openRecieptDialog() {
JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PosRecieptDialog posRecieptDialog = new PosRecieptDialog(reciept);
        posRecieptDialog.setLocationRelativeTo(this);
        posRecieptDialog.setVisible(true);
    }

    private void clearReciept() {
        reciept = new Reciept();
                
        reciept.setEmployee(EmployeeService.getCurrentUser());
        refreshReciept();
    }

    public void openSearchMemDialog() {
       Member editedMember = new Member();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
       SearchMemDialog userDialog = new SearchMemDialog(frame, editedMember);
        userDialog.setLocationRelativeTo(this);
      userDialog.setVisible(true);
    }

    private void refreshReciept() {
        lastprice =reciept.getTotal();
        tblRecieptDetail.revalidate();
        tblRecieptDetail.repaint();
        txSubtotal.setText("" + reciept.getTotal());
//        lblDiscount.setText("Discount: ");
        totalprice = reciept.getTotal()-totaldiscount;
        txTotal.setText(" " + totalprice);
        txPointdiscount.setText("Point Discount : -");
         lablepromoname.setText("Promotion name : -");
        ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
        for (RecieptDetail r : recieptDetails) {
            System.out.println(r);
        }

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel asd;
    private javax.swing.JLabel asdasda;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JButton btnMember;
    private javax.swing.JButton btnMemberAdd;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lablepromoname;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblRecieptDetail;
    private javax.swing.JLabel txDiscount;
    private javax.swing.JLabel txPointdiscount;
    private javax.swing.JLabel txSubtotal;
    private javax.swing.JLabel txTotal;
    private javax.swing.JLabel txdoasdas;
    private javax.swing.JButton txpromoname;
    private javax.swing.JLabel txsubtotal;
    // End of variables declaration//GEN-END:variables
@Override
    public void buy(Product product, int qty) {
        reciept.addRecieptDetail(product, qty);
        refreshReciept();
    }

    @Override
    public void buy(Product product, int qty, String type, String sweet) {
        reciept.addRecieptDetail(product, qty, type, sweet);
        refreshReciept();
    }
}

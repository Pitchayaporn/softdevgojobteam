/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class ProductSellReport {
    private int id;
    private String name;
    private int totalQuantity;
    private float totalPrice;

    public ProductSellReport(int id, String name, int totalQuantity, float totalPrice) {
        this.id = id;
        this.name = name;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
    }

    public ProductSellReport(String name, int totalQuantity, float totalPrice) {
        this.id = -1;
        this.name = name;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
    }

    public ProductSellReport() {
        this(-1, "", 0, 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "ProductSellReport{" + "id=" + id + ", name=" + name + ", totalQuantity=" + totalQuantity + ", totalPrice=" + totalPrice + '}';
    }
    
    public static ProductSellReport fromRS(ResultSet rs) {
        ProductSellReport obj = new ProductSellReport();
        try {
            obj.setId(rs.getInt("PD_ID"));
            obj.setName(rs.getString("PD_name"));
            obj.setTotalQuantity(rs.getInt("TotalQuantity"));
            obj.setTotalPrice(rs.getFloat("TotalPrice"));
            

        } catch (SQLException ex) {
            Logger.getLogger(ProductSellReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }

    
}

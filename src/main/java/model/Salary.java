/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pasinee
 */
public class Salary {

    private int id;
    private int empid;
    private Date sldate;
    private int totalhr;
    private int ratehr;
    private int netincome;
    private String fname;
    private String lname;
    private int pos;

    public Salary(int id, int empid, Date sldate, int totalhr, int ratehr, int netincome, String fname, String lname, int pos) {
        this.id = id;
        this.empid = empid;
        this.sldate = sldate;
        this.totalhr = totalhr;
        this.ratehr = ratehr;
        this.netincome = netincome;
        this.fname = fname;
        this.lname = lname;
        this.pos = pos;
    }

    public Salary(int empid, Date sldate, int totalhr, int ratehr, int netincome, String fname, String lname, int pos) {
        this.id = -1;
        this.empid = empid;
        this.sldate = sldate;
        this.totalhr = totalhr;
        this.ratehr = ratehr;
        this.netincome = netincome;
        this.fname = fname;
        this.lname = lname;
        this.pos = pos;
    }
        public Salary() {
        this.id = -1;
        this.empid = 0;
        this.sldate = null;
        this.totalhr = 0;
        this.ratehr = 0;
        this.netincome = 0;
        this.fname = "";
        this.lname = "";
        this.pos = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public Date getSldate() {
        return sldate;
    }

    public void setSldate(Date sldate) {
        this.sldate = sldate;
    }

    public int getTotalhr() {
        return totalhr;
    }

    public void setTotalhr(int totalhr) {
        this.totalhr = totalhr;
    }

    public int getRatehr() {
        return ratehr;
    }

    public void setRatehr(int ratehr) {
        this.ratehr = ratehr;
    }

    public int getNetincome() {
        return netincome;
    }

    public void setNetincome(int netincome) {
        this.netincome = netincome;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", empid=" + empid + ", fname=" + fname + ", lname=" + lname + ", sldate=" + sldate + ", totalhr=" + totalhr + ", ratehr=" + ratehr + ", netincome=" + netincome + ", pos=" + pos + '}';
    }

    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setId(rs.getInt("sl_id"));
            salary.setEmpid(rs.getInt("emp_id"));
            salary.setSldate(rs.getDate("sl_date"));
            salary.setTotalhr(rs.getInt("sl_total_hr"));
            salary.setRatehr(rs.getInt("sl_rate_hr"));
            salary.setNetincome(rs.getInt("sl_net_income"));

            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(salary.getEmpid());
            String fname = employee.getFname();
            salary.setFname(fname);
            String lname = employee.getLname();
            salary.setLname(lname);
            int pos = employee.getPos();
            salary.setPos(pos);

        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }

    public Salary get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arthaphan
 */
public class Category {
    private int id;
    private String type;

    @Override
    public String toString() {
        return "Category{" + "id=" + id + ", type=" + type + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

 
     public Category(){
         this.id = -1;
    }
      public static Category fromRS(ResultSet rs) {
        Category cat = new Category();
        try {
            cat.setId(rs.getInt("ctgr_id"));
            cat.setType(rs.getString("ctgr_name"));
    
        } catch (SQLException ex) {
            Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cat;
    }

   

}

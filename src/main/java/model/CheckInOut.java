/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pasinee
 */
public class CheckInOut {

    private int id;
    private int empId;
    private Date checkDate;
    private Time checkIn;
    private Time checkOut;
    private String checkStatus;
    private int totalHr;

    //private int workHr;
    //private String checkInStr;
    //private String checkOutStr;
    //private String checkDateStr;

//    EmployeeDao employeeDao = new EmployeeDao();
    //SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
    //SimpleDateFormat formatterHrs = new SimpleDateFormat("HH:mm:ss");
    //SimpleDateFormat formatterHH = new SimpleDateFormat("HH");

    public CheckInOut(int id, int empId, Date checkDate, Time checkIn, Time checkOut, String checkStatus, int totalHr) {
        this.id = id;
        this.empId = empId;
        this.checkDate = checkDate;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.checkStatus = checkStatus;
        this.totalHr = totalHr;
    }

    public CheckInOut(int empId) {
        this.id = -1;
        this.empId = empId;
        this.checkDate = null;
        this.checkIn = null;
        this.checkOut = null;
        this.checkStatus = null;
        this.totalHr = 0;
    }

    public CheckInOut() {
        this.id = -1;
        this.empId = 0;
        this.checkDate = null;
        this.checkIn = null;
        this.checkOut = null;
        this.checkStatus = null;
        this.totalHr = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public Time getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Time checkIn) {
        this.checkIn = checkIn;
    }

    public Time getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Time checkOut) {
        this.checkOut = checkOut;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public int getTotalHr() {
        return totalHr;
    }

    public void setTotalHr(int totalHr) {
        this.totalHr = totalHr;
    }

    

    //public static Date parseSQLiteTimestamp(String timestamp) throws ParseException {
    //    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    //    return sdf.parse(timestamp);
    //}

    

    @Override
    public String toString() {
        return "CheckInOut{" + "id=" + id + ", empId=" + empId + ", checkDate=" + checkDate + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", checkStatus=" + checkStatus + ", totalHr=" + totalHr+'}';
    }

    public static CheckInOut fromRS(ResultSet rs) {
        CheckInOut cio = new CheckInOut();
        Employee emp = new Employee();
        try {
            cio.setId(rs.getInt("check_in_out_id"));
            cio.setEmpId(rs.getInt("emp_id"));
            cio.setCheckDate(rs.getDate("check_date"));
            cio.setCheckIn(rs.getTime("check_in"));
            cio.setCheckOut(rs.getTime("check_out"));
            cio.setCheckStatus(rs.getString("check_status"));
            cio.setTotalHr(rs.getInt("total_hr"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cio;
    }
}



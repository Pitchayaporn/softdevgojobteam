/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PT
 */
public class CheckMaterial {
    private int id;
    private int empid;
    private String fname;
    private String lname;
    private Date date;
    
    //All
    public CheckMaterial(int id, int empid, String fname, String lname, Date date) {
        this.id = id;
        this.empid = empid;
        this.fname = fname;
        this.lname = lname;
        this.date = date;
    }
    
    
    //No id
    public CheckMaterial(int empid, String fname, String lname, Date date) {
        this.id = -1;
        this.empid = empid;
        this.fname = fname;
        this.lname = lname;
        this.date = date;
    }
    
    //No id,date
    public CheckMaterial(int empid, String fname, String lname) {
        this.id = -1;
        this.empid = empid;
        this.fname = fname;
        this.lname = lname;
    }
    
    //No All
    public CheckMaterial() {
        this.id = -1;
        this.empid = 0;
        this.fname = "";
        this.lname = "";
        this.date = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CheckMaterial{" + "id=" + id + ", empid=" + empid + ", fname=" + fname + ", lname=" + lname + ", date=" + date + '}';
    }
    
    public static CheckMaterial fromRS(ResultSet rs) {
        CheckMaterial checkmaterial = new CheckMaterial();
        try {
            checkmaterial.setId(rs.getInt("checkmaterial_id"));
            checkmaterial.setEmpid(rs.getInt("emp_id"));
            checkmaterial.setFname(rs.getString("emp_fname"));
            checkmaterial.setLname(rs.getString("emp_lname"));
            checkmaterial.setDate(rs.getDate("checkmaterial_date"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkmaterial;
    }
}

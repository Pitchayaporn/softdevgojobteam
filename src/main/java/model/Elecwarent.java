/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;


import dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class Elecwarent {

    private int id;
    private String name;
    private java.util.Date date;
    private double price;
    private int unit;
    private String status;
    private int emp_id;
    private Employee employee;

    public Elecwarent(int id, String name, Date date, double price, int unit, String status, int emp_id) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.price = price;
        this.unit = unit;
        this.status = status;
        this.emp_id = emp_id;
    }

    public Elecwarent(String name, Date date, double price, int unit, String status, int emp_id) {
        this.id = -1;
        this.name = name;
        this.date = date;
        this.price = price;
        this.unit = unit;
        this.status = status;
        this.emp_id = emp_id;
    }

    public Elecwarent(String name, double price, int unit, String status, int emp_id) {
        this.id = -1;
        this.name = name;
        this.date = null;
        this.price = price;
        this.unit = unit;
        this.status = status;
        this.emp_id = emp_id;
    }

    public Elecwarent() {
        this.id = -1;
        this.name = "";
        this.date = null;
        this.price = 0;
        this.unit = 0;
        this.status = "";
        this.emp_id = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.emp_id = employee.getId();
    }

    @Override
    public String toString() {
        return "Elecwarent{" + "id=" + id + ", name=" + name + ", date=" + date + ", price=" + price + ", unit=" + unit + ", status=" + status + ", emp_id=" + emp_id + ", employee=" + employee + '}';
    }

    public static Elecwarent fromRS(ResultSet rs) {
        Elecwarent ewr = new Elecwarent();
        try {
            ewr.setId(rs.getInt("elecwarent_id"));
            ewr.setName(rs.getString("elecwarent_name"));
            ewr.setDate(rs.getTimestamp("elecwarent_date"));
            ewr.setPrice(rs.getDouble("elecwarent_price"));
            ewr.setUnit(rs.getInt("elecwarent_unit"));
            ewr.setStatus(rs.getString("elecwarent_status"));
            ewr.setEmp_id(rs.getInt("emp_id"));
            //            Populattion
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(ewr.getEmp_id());
            ewr.setEmployee(employee);
        } catch (SQLException ex) {
            Logger.getLogger(Elecwarent.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ewr;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PT
 */
public class CheckMaterialDetail {
    private int id;
    private int matid;
    private String matname;
    private String status;
    private int qty;
    private String dateexp;
    private int min;
    private String unit;
    private Date lastdate;

    public CheckMaterialDetail(int id, int matid, String matname, String status, int qty, String dateexp, int min, String unit, Date lastdate) {
        this.id = id;
        this.matid = matid;
        this.matname = matname;
        this.status = status;
        this.qty = qty;
        this.dateexp = dateexp;
        this.min = min;
        this.unit = unit;
        this.lastdate = lastdate;
    }
    
    public CheckMaterialDetail(int id, int matid, String matname, String status, int qty, String dateexp, int min, String unit) {
        this.id = id;
        this.matid = matid;
        this.matname = matname;
        this.status = status;
        this.qty = qty;
        this.dateexp = dateexp;
        this.min = min;
        this.unit = unit;
    }
    
    public CheckMaterialDetail(int matid, String matname, String status, int qty, String dateexp, int min, String unit, Date lastdate) {
        this.id = -1;
        this.matid = matid;
        this.matname = matname;
        this.status = status;
        this.qty = qty;
        this.dateexp = dateexp;
        this.min = min;
        this.unit = unit;
        this.lastdate = lastdate;
    }
    
    public CheckMaterialDetail(int matid, String matname, String status, int qty, String dateexp, int min, String unit) {
        this.id = -1;
        this.matid = matid;
        this.matname = matname;
        this.status = status;
        this.qty = qty;
        this.dateexp = dateexp;
        this.min = min;
        this.unit = unit;
    }
    
    public CheckMaterialDetail() {
        this.id = -1;
        this.matid = 0;
        this.matname = "";
        this.status = "";
        this.qty = 0;
        this.dateexp = "";
        this.min = 0;
        this.unit = "";
        this.lastdate = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatid() {
        return matid;
    }

    public void setMatid(int matid) {
        this.matid = matid;
    }

    public String getMatname() {
        return matname;
    }

    public void setMatname(String matname) {
        this.matname = matname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getDateexp() {
        return dateexp;
    }

    public void setDateexp(String dateexp) {
        this.dateexp = dateexp;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Date getLastdate() {
        return lastdate;
    }

    public void setLastdate(Date lastdate) {
        this.lastdate = lastdate;
    }

    @Override
    public String toString() {
        return "CheckMaterialDetail{" + "id=" + id + ", matid=" + matid + ", matname=" + matname + ", status=" + status + ", qty=" + qty + ", dateexp=" + dateexp + ", min=" + min + ", unit=" + unit + ", lastdate=" + lastdate + '}';
    }
    
    public static CheckMaterialDetail fromRS(ResultSet rs) {
        CheckMaterialDetail checkmaterial = new CheckMaterialDetail();
        try {
            checkmaterial.setId(rs.getInt("checkmaterialdetail_id"));
            checkmaterial.setMatid(rs.getInt("material_id"));
            checkmaterial.setMatname(rs.getString("material_name"));
            checkmaterial.setStatus(rs.getString("status"));
            checkmaterial.setQty(rs.getInt("qty"));
            checkmaterial.setDateexp(rs.getString("date_exp"));
            checkmaterial.setMin(rs.getInt("min"));
            checkmaterial.setUnit(rs.getString("unit"));
            checkmaterial.setLastdate(rs.getDate("last_update"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterialDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkmaterial;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterial {
    private int bill_id;
    private int emp_id;
    private String ven_name;
    private float bill_total_price;
    private float bill_received;
    private float bill_change;
    private Date bill_pur_date;
    private float bill_discount;
    private float bill_net_price;
    private ArrayList<BillMaterialDetails> billmaterialdetails = new ArrayList<>();
    public BillMaterialDetails getbillmaterialdetails;

    public BillMaterial(int bill_id, int emp_id, String ven_name, float bill_total_price, float bill_received, float bill_change, Date bill_pur_date,float bill_discount, float bill_net_price) {
        this.bill_id = bill_id;
        this.emp_id = emp_id;
        this.ven_name = ven_name;
        this.bill_total_price = bill_total_price;
        this.bill_received = bill_received;
        this.bill_change = bill_change;
        this.bill_pur_date = bill_pur_date;
        this.bill_discount = bill_discount;
        this.bill_net_price = bill_net_price;
    }
    public BillMaterial(int emp_id, String ven_name, float bill_total_price, float bill_received, float bill_change, Date bill_pur_date, float bill_discount, float bill_net_price) {
        this.bill_id = -1;
        this.emp_id = emp_id;
        this.ven_name = ven_name;
        this.bill_total_price = bill_total_price;
        this.bill_received = bill_received;
        this.bill_change = bill_change;
        this.bill_pur_date = bill_pur_date;
        this.bill_discount = bill_discount;
        this.bill_net_price = bill_net_price;
    }
    public BillMaterial(int emp_id,String ven_name, float bill_total_price, float bill_received, float bill_change,float bill_discount, float bill_net_price) {
        this.bill_id = -1;
        this.emp_id = emp_id;
        this.ven_name = ven_name;
        this.bill_total_price = bill_total_price;
        this.bill_received = bill_received;
        this.bill_change = bill_change;
        this.bill_pur_date = null;
        this.bill_discount = bill_discount;
        this.bill_net_price = bill_net_price;
    }
    
     public BillMaterial(String ven_name, float bill_total_price, float bill_received, float bill_change, Date bill_pur_date, Date bill_rec_date, float bill_discount, float bill_net_price) {
        this.bill_id = -1;
        this.emp_id = 0;
        this.ven_name = ven_name;
        this.bill_total_price = bill_total_price;
        this.bill_received = bill_received;
        this.bill_change = bill_change;
        this.bill_pur_date = bill_pur_date;
        this.bill_discount = bill_discount;
        this.bill_net_price = bill_net_price;
    }
     
    public BillMaterial(String ven_name, float bill_total_price, float bill_received, float bill_change, Date bill_rec_date, float bill_discount, float bill_net_price) {
        this.bill_id = -1;
        this.emp_id = 0;
        this.ven_name = ven_name;
        this.bill_total_price = bill_total_price;
        this.bill_received = bill_received;
        this.bill_change = bill_change;
        this.bill_pur_date = null;
        this.bill_discount = bill_discount;
        this.bill_net_price = bill_net_price;
    }
     public BillMaterial() {
        this.bill_id = -1;
        this.emp_id = 0;
        this.ven_name = "";
        this.bill_total_price = 0;
        this.bill_received = 0;
        this.bill_change = 0;
        this.bill_pur_date = null;
        this.bill_discount = 0;
        this.bill_net_price = 0;
    }


    public int getBill_id() {
        return bill_id;
    }

    public void setBill_id(int bill_id) {
        this.bill_id = bill_id;
    }

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public String getVen_name() {
        return ven_name;
    }

    public void setVen_name(String ven_name) {
        this.ven_name = ven_name;
    }

    public float getBill_total_price() {
        return bill_total_price;
    }

    public void setBill_total_price(float bill_total_price) {
        this.bill_total_price = bill_total_price;
    }

    public float getBill_received() {
        return bill_received;
    }

    public void setBill_received(float bill_received) {
        this.bill_received = bill_received;
    }

    public float getBill_change() {
        return bill_change;
    }

    public void setBill_change(float bill_change) {
        this.bill_change = bill_change;
    }

    public Date getBill_pur_date() {
        return bill_pur_date;
    }

    public void setBill_pur_date(Date bill_pur_date) {
        this.bill_pur_date = bill_pur_date;
    }
    
    public float getBill_discount() {
        return bill_discount;
    }

    public void setBill_discount(float bill_discount) {
        this.bill_discount = bill_discount;
    }

    public float getBill_net_price() {
        return bill_net_price;
    }

    public void setBill_net_price(float bill_net_price) {
        this.bill_net_price = bill_net_price;
    }
    public ArrayList<BillMaterialDetails> getBillMaterialDetailses() {
        return billmaterialdetails;
    }

    public void setBillMaterialDetails(ArrayList<BillMaterialDetails> billMaterialDetailses) {
        this.billmaterialdetails = billMaterialDetailses;
    
    }

    @Override
    public String toString() {
        return "BillMaterial{" + "bill_id=" + bill_id + ", emp_id=" + emp_id + ", ven_name=" + ven_name + ", bill_total_price=" + bill_total_price + ", bill_received=" + bill_received + ", bill_change=" + bill_change + ", bill_pur_date=" + bill_pur_date + ", bill_discount=" + bill_discount + ", bill_net_price=" + bill_net_price + '}';
    }
    public void addBillMaterialDetails(BillMaterialDetails billMaterialDetails) {
       billmaterialdetails.add(billMaterialDetails);
        calculateTotal();
    }
    
    public void updateBillMaterialDetails(BillMaterialDetails billMaterialDetails, int index) {
        billmaterialdetails.set(index, billMaterialDetails);
        calculateTotal();
    }

    public void delBillMaterialDetails(BillMaterialDetails billMaterialDetails) {
        billmaterialdetails.remove(billMaterialDetails);
        calculateTotal();
    }

    public void calculateTotal() {
        float allTotal = 0.0f;
        for (BillMaterialDetails billMaterialDetails : billmaterialdetails) {
            allTotal += billMaterialDetails.getBilld_total_price();
        }
        this.bill_net_price = allTotal;
    }

    public static BillMaterial fromRS(ResultSet rs){
        BillMaterial billmaterial = new BillMaterial();
        try {
            billmaterial.setBill_id(rs.getInt("bill_id"));
            billmaterial.setEmp_id(rs.getInt("emp_id"));
            billmaterial.setVen_name(rs.getString("ven_name"));
            billmaterial.setBill_total_price(rs.getFloat("bill_total_price"));
            billmaterial.setBill_received(rs.getFloat("bill_received"));
            billmaterial.setBill_change(rs.getFloat("bill_change"));
            billmaterial.setBill_pur_date(rs.getDate("bill_pur_date"));
            billmaterial.setBill_discount(rs.getFloat("bill_discount"));
            billmaterial.setBill_net_price(rs.getFloat("bill_net_price"));
        } catch (SQLException ex) {
            Logger.getLogger(BillMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billmaterial;
    } 


    }
    
    


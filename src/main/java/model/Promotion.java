/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wittaya5329
 */
public class Promotion {
    private int id;
    private String name;
    private int percent;
    private int condition;
    private int status;

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", percent=" + percent + ", condition=" + condition + ", status=" + status + '}';
    }
    

    public Promotion(int id, String name, int percent, int condition, int status) {
        this.id = id;
        this.name = name;
        this.percent = percent;
        this.condition = condition;
        this.status = status;
    }

    public Promotion(String name, int percent, int condition, int status) {
        this.id = -1;
        this.name = name;
        this.percent = percent;
        this.condition = condition;
        this.status = status;
    }
    public Promotion() {
        this.id = -1;
        this.name = "";
        this.percent = 0;
        this.condition = 0;
        this.status = 0;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }

    
    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("pmt_Id"));
            promotion.setName(rs.getString("pmt_name"));
            promotion.setPercent(rs.getInt("pmt_discount"));
            promotion.setCondition(rs.getInt("pmt_condition"));
            promotion.setStatus(rs.getInt("pmt_status"));
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }
}

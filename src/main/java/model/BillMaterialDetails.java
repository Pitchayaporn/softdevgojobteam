/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterialDetails {
    private int bill_detail_id;
    private int material_id;
    private int bill_id;
    private int billd_quan;
    private float billd_total_price;

    public BillMaterialDetails(int bill_detail_id, int material_id, int bill_id, int billd_quan, float billd_total_price) {
        this.bill_detail_id = bill_detail_id;
        this.material_id = material_id;
        this.bill_id = bill_id;
        this.billd_quan = billd_quan;
        this.billd_total_price = billd_total_price;
    }
     public BillMaterialDetails(int material_id, int bill_id, int billd_quan, float billd_total_price) {
        this.bill_detail_id = -1;
        this.material_id = material_id;
        this.bill_id = bill_id;
        this.billd_quan = billd_quan;
        this.billd_total_price = billd_total_price;
    }
      public BillMaterialDetails(int bill_id, int billd_quan, float billd_total_price) {
        this.bill_detail_id = -1;
        this.material_id = -1;
        this.bill_id = bill_id;
        this.billd_quan = billd_quan;
        this.billd_total_price = billd_total_price;
    }
    public BillMaterialDetails(int billd_quan, float billd_total_price) {
        this.bill_detail_id = -1;
        this.material_id = -1;
        this.bill_id = -1;
        this.billd_quan = billd_quan;
        this.billd_total_price = billd_total_price;
    }
    public BillMaterialDetails() {
        this.bill_detail_id = -1;
        this.material_id = -1;
        this.bill_id = -1;
        this.billd_quan = 0;
        this.billd_total_price = 0;
    }

    public int getBill_detail_id() {
        return bill_detail_id;
    }

    public void setBill_detail_id(int bill_detail_id) {
        this.bill_detail_id = bill_detail_id;
    }

    public int getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(int material_id) {
        this.material_id = material_id;
    }

    public int getBill_id() {
        return bill_id;
    }

    public void setBill_id(int bill_id) {
        this.bill_id = bill_id;
    }

    public int getBilld_quan() {
        return billd_quan;
    }

    public void setBilld_quan(int billd_quan) {
        this.billd_quan = billd_quan;
    }

    public float getBilld_total_price() {
        return billd_total_price;
    }

    public void setBilld_total_price(float billd_total_price) {
        this.billd_total_price = billd_total_price;
    }

    @Override
    public String toString() {
        return "BillMaterialDetails{" + "bill_detail_id=" + bill_detail_id + ", material_id=" + material_id + ", bill_id=" + bill_id + ", billd_quan=" + billd_quan + ", billd_total_price=" + billd_total_price + '}';
    }
     public static BillMaterialDetails fromRS(ResultSet rs){
        BillMaterialDetails billmaterialdetail = new BillMaterialDetails();
        try {
            billmaterialdetail.setBill_detail_id(rs.getInt("bill_detail_id"));
            billmaterialdetail.setMaterial_id(rs.getInt("material_id"));
            billmaterialdetail.setBill_id(rs.getInt("bill_id"));
            billmaterialdetail.setBilld_quan(rs.getInt("billd_quan"));
            billmaterialdetail.setBilld_total_price(rs.getFloat("billd_total_price"));
            
        } catch (SQLException ex) {
            Logger.getLogger(BillMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billmaterialdetail;
}
}
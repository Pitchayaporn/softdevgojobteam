/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import dao.BillMaterialDao;
import dao.BillMaterialDetailsDao;

import java.util.ArrayList;
import java.util.List;
import model.BillMaterial;
import model.BillMaterialDetails;
import service.BillMaterialService;

/**
 *
 * @author Paweena Chinasri
 */
public class TestBillMaterialDao {

    public static void main(String[] args) {
        BillMaterialDao billMaterialDao = new BillMaterialDao();
        for (BillMaterial e : billMaterialDao.getAll()) {
            System.out.println(e);
        }
        
    }
}

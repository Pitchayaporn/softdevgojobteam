/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import java.text.ParseException;
import model.CheckInOut;
import service.CheckInOutService;

/**
 *
 * @author pasinee
 */
public class TestCheckInOutService {

     public static void main(String[] args) throws ParseException {
        CheckInOutService cio = new CheckInOutService();
        for (CheckInOut checkInOut : cio.getCheckInOuts()) {
            System.out.println(checkInOut);
        }
        System.out.println("-----------");
        //CheckInOut cio1 = cio.getById(1);
        CheckInOut cio2 = cio.getById(2);
        CheckInOut cio3 = new CheckInOut(1);
        //cio2.setCheckStatus("Intime");
        //System.out.println(cio1);
        System.out.println(cio2);
        //System.out.println(cio3);
        
        //cio.updateLogout(cio1);//total
        //cio.update(cio2);//
        //cio.addNew(cio3);
        cio.updateStatus(cio2);//

        //cio.delete(cio3);
        System.out.println("-----------");
        for (CheckInOut checkInOut : cio.getCheckInOuts()) {
            System.out.println(checkInOut);
        }
    }
}

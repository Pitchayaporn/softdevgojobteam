/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;


import dao.BillMaterialDetailsDao;
import java.util.ArrayList;
import java.util.List;

import model.BillMaterialDetails;


/**
 *
 * @author Paweena Chinasri
 */
public class TestBillMaterialDetailsDao {
     public static void main(String[] args) {
        BillMaterialDetailsDao billMaterialDetailsDao = new BillMaterialDetailsDao();
        for(BillMaterialDetails e: billMaterialDetailsDao.getAll()) {
            System.out.println(e);
        }
    }
}

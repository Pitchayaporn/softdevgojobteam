/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.BillMaterialDao;
import dao.BillMaterialDetailsDao;
import java.util.List;
import model.BillMaterialDetails;

/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterialDetailsService {
    
     public BillMaterialDetails getById(int id) {
        BillMaterialDetailsDao billMaterialDetailsDao = new BillMaterialDetailsDao();
        return billMaterialDetailsDao.get(id);
    }
    public List<BillMaterialDetails> getBillMaterialDetails() {
        BillMaterialDetailsDao billMaterialDetailsDao = new BillMaterialDetailsDao();
        return billMaterialDetailsDao.getAll(" bill_detail_id asc");
    }
    public BillMaterialDetails addNew(BillMaterialDetails editedBillMaterialDetails) {
        BillMaterialDetailsDao billMaterialDetailsDao = new BillMaterialDetailsDao();
        return billMaterialDetailsDao.save(editedBillMaterialDetails);
    }

    public BillMaterialDetails update(BillMaterialDetails editedBillMaterialDetails) {
        BillMaterialDetailsDao billMaterialDetailsDao = new BillMaterialDetailsDao();
        return billMaterialDetailsDao.update(editedBillMaterialDetails);
    }

    public int delete(BillMaterialDetails editedBillMaterialDetails) {
        BillMaterialDetailsDao billMaterialDetailsDao = new BillMaterialDetailsDao();
        return billMaterialDetailsDao.delete(editedBillMaterialDetails);
    }
}

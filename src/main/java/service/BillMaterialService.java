/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.BillMaterialDao;
import java.util.ArrayList;
import java.util.List;
import model.BillMaterial;

/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterialService {
    
    public BillMaterial getById(int id) {
        BillMaterialDao billmaterialDao = new BillMaterialDao();
        return billmaterialDao.get(id);
    }
    public List<BillMaterial> getBillMaterial() {
        BillMaterialDao billmaterialDao = new BillMaterialDao();
        return billmaterialDao.getAll(" bill_id asc");
    }
    public BillMaterial addNew(BillMaterial editedBillMaterial) {
        BillMaterialDao billmaterialDao = new BillMaterialDao();
        return billmaterialDao.save(editedBillMaterial);
    }

    public BillMaterial update(BillMaterial editedBillMaterial) {
        BillMaterialDao billmaterialDao = new BillMaterialDao();
        return billmaterialDao.update(editedBillMaterial);
    }
     public ArrayList<BillMaterial> getAll() {
        ArrayList<BillMaterial> billmats;
        BillMaterialDao billmaterialDao = new BillMaterialDao();
        billmats = (ArrayList<BillMaterial>) billmaterialDao.getAll("bill_id");

        return billmats;
    }

    public int delete(BillMaterial editedBillMaterial) {
        BillMaterialDao billmaterialDao = new BillMaterialDao();
        return billmaterialDao.delete(editedBillMaterial);
    }
}

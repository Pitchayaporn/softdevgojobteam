/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.CheckMaterialDao;
import java.util.List;
import model.CheckMaterial;

/**
 *
 * @author PT
 */
public class CheckMaterialService {
    public List<CheckMaterial> getCheckMaterial(){
        CheckMaterialDao  checkmaterialdao = new CheckMaterialDao();
        return checkmaterialdao.getAll(" checkmaterial_id asc");
    }
    
    public CheckMaterial addNew(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkmaterialdao = new CheckMaterialDao();
        return checkmaterialdao.save(editedCheckMaterial);
    }

    public CheckMaterial update(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkmaterialdao = new CheckMaterialDao();
        return checkmaterialdao.update(editedCheckMaterial);
    }

    public int delete(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkmaterialdao = new CheckMaterialDao();
        return checkmaterialdao.delete(editedCheckMaterial);
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;


import dao.ProductDao;
import java.util.ArrayList;
import java.util.List;
import model.Product;

/**
 *
 * @author user
 */
public class ProductService {
    private ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductOrderByName() {
        return (ArrayList<Product>) productDao.getAll(" pd_name ASC ");
    }
    public Product getById(int id) {
        ProductDao categoryDao = new  ProductDao();
        return categoryDao.get(id);
    }
    
public List<Product> getStatus(String status) {
        ProductDao productDao = new ProductDao();
        return productDao.getAll("WHERE ctgr_id = '" + status + "'", "ORDER BY pd_id, pd_name, pd_price,ctgr_id,pd_size,pd_sweetlevel ASC");
    }
    public List<Product> getProducts() {
        ProductDao categoryDao = new ProductDao();
        return categoryDao.getAll(" ctgr_id ASC");
    }

    public Product addNew(Product editedProduct) {
        ProductDao categoryDao = new ProductDao();
        return categoryDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao categoryDao = new ProductDao();
        return categoryDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao categoryDao = new ProductDao();
        return categoryDao.delete(editedProduct);
    }
    
    
}

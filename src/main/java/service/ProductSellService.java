/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import java.util.List;
import dao.ProductSellDao;
import model.ProductSellReport;

/**
 *
 * @author HP
 */
public class ProductSellService {
    public List<ProductSellReport> getProductSellByTotalPrice() {
        ProductSellDao productSellDao = new ProductSellDao();
        return productSellDao.getProductSellByTotalPrice();
    }
}

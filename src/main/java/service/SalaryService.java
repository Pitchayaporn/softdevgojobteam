/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.SalaryDao;
import java.util.List;
import model.Salary;

/**
 *
 * @author pasinee
 */
public class SalaryService {
    public Salary getById(int id) {
        SalaryDao SalaryDao = new SalaryDao();
        Salary Salary = SalaryDao.get(id);
        return Salary;
    }
    
    public List<Salary> getSalarys(){
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.getAll(" sl_id ASC");
    }

    public Salary addNew(Salary editedSalary) {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.save(editedSalary);
    }

    public Salary update(Salary editedSalary) {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.update(editedSalary);
    }

    public int delete(Salary editedSalary) {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.delete(editedSalary);
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;


import dao.MemberDao;
import java.util.List;
import model.Member;

/**
 *
 * @author Arthaphan
 */
public class MemberService {
     public static Member currentMember;

    public static void setCurrentMember(Member currentMember) {
        MemberService.currentMember = currentMember;
    }
     
    
    public static Member getCurrentMember() {
        return  currentMember;
    }
    public Member getByPhone(String phone) {
        MemberDao memberDao = new MemberDao();
        Member member = memberDao.getByTel(phone);
        return member;
    }
    
    public List<Member> getCategories(){
        MemberDao memberDao = new MemberDao();
        return memberDao.getAll(" mem_id ASC");
    }

    public Member addNew(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.save(editedMember);
    }

    public Member update(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.update(editedMember);
    }

    public int delete(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.delete(editedMember);
    }
    
}

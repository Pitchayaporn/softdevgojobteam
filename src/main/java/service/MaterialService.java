/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.MaterialDao;
import java.util.List;
import model.MaterialReport;

/**
 *
 * @author HP
 */
public class MaterialService {
    public List<MaterialReport> getFewProductsLeft() {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getFewProductsLeft();
    }
}

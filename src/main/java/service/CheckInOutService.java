/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.CheckInOutDao;
import java.text.ParseException;
import java.util.List;
import model.CheckInOut;

/**
 *
 * @author pasinee
 */
public class CheckInOutService {
    
   public CheckInOut getById(int id) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.get(id);
    }
   
   public CheckInOut getByEmp(int id) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getByEmpId(id);
    }

    public List<CheckInOut> getCheckInOuts(){
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getAll(" check_in_out_id desc");
    }

    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.save(editedCheckInOut);
    }
    
    public CheckInOut updateStatus(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.updateStu(editedCheckInOut);
    }
    
    public CheckInOut Last() {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getLast();
    }

    //
    public CheckInOut update(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.update(editedCheckInOut);
    }
    
    public CheckInOut updateLogout(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.updateLogout(editedCheckInOut);
    }

    public int delete(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.delete(editedCheckInOut);
    }
    
    
    
    
    
    
    
    
    
    
    
    

}

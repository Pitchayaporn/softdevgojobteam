/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.PromotionDao;
import java.util.List;
import model.Promotion;

/**
 *
 * @author werapan
 */
public class PromotionService {
    private static Promotion currentPromotion;

    public static Promotion getCurrentPromotion() {
        return currentPromotion;
    }

    public static void setCurrentPromotion(Promotion currentPromotion) {
        PromotionService.currentPromotion = currentPromotion;
    }
    
    
    public Promotion getById(int id) {
        PromotionDao promotionDao = new  PromotionDao();
        return promotionDao.get(id);
    }

    public List<Promotion> getPromotions() {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll(" pmt_Id asc");
    }

    public Promotion addNew(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.save(editedPromotion);
    }

    public Promotion update(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.delete(editedPromotion);
    }
}

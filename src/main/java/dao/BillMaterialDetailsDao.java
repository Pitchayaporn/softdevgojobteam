/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import model.BillMaterialDetails;

/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterialDetailsDao {
    
    
    public BillMaterialDetails get(int id) {
        BillMaterialDetails billmaterialdetails = null;
        String sql = "SELECT * FROM BILL_MATERIAL_DETAILS WHERE bill_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                billmaterialdetails = BillMaterialDetails.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billmaterialdetails;
    }
    
     public List<BillMaterialDetails> getAll() {
        ArrayList<BillMaterialDetails> list = new ArrayList();
        String sql = "SELECT * FROM BILL_MATERIAL_DETAILS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterialDetails billMaterialDetails = BillMaterialDetails.fromRS(rs);
//                list.add(billMaterial);
                billMaterialDetails.setBill_detail_id(rs.getInt("bill_detail_id"));
                billMaterialDetails.setMaterial_id(rs.getInt("material_id"));
                billMaterialDetails.setBill_id(rs.getInt("bill_id"));
                billMaterialDetails.setBilld_quan(rs.getInt("billd_quan"));
                billMaterialDetails.setBilld_total_price(rs.getFloat("billd_total_price"));
               
                list.add(billMaterialDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
        
    }
      public BillMaterialDetails save(BillMaterialDetails obj) {
        String sql = "INSERT INTO BILL_MATERIAL_DETAILS (material_id, bill_id, billd_quan, billd_total_price)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
         try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMaterial_id());
            stmt.setInt(2, obj.getBill_id());
            stmt.setInt(3, obj.getBilld_quan());
            stmt.setFloat(4,obj.getBilld_total_price() );
            
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setBill_id(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
       
    public BillMaterialDetails update(BillMaterialDetails obj) {
        String sql = "UPDATE BILL_MATERIAL_DETAILS"
                + " SET material_id = ?, bill_id = ?, billd_quan = ?, billd_total_price = ?"
                + " WHERE bill_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMaterial_id());
            stmt.setInt(2, obj.getBill_id());
            stmt.setInt(3, obj.getBilld_quan());
            stmt.setFloat(4, obj.getBilld_total_price());
           
            stmt.setInt(5, obj.getBill_id());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    public int delete(BillMaterialDetails obj) {
        String sql = "DELETE FROM BILL_MATERIAL_DETAILS WHERE bill_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBill_id());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    public List<BillMaterialDetails> getAll(String where, String order) {
        ArrayList<BillMaterialDetails> list = new ArrayList();
        String sql = "SELECT * FROM BILL_MATERIAL_DETAILS where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterialDetails billmaterialdetails = BillMaterialDetails.fromRS(rs);
                list.add(billmaterialdetails);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<BillMaterialDetails> getAll(String order) {
        ArrayList<BillMaterialDetails> list = new ArrayList();
        String sql = "SELECT * FROM BILL_MATERIAL_DETAILS ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterialDetails billmaterialdetails = BillMaterialDetails.fromRS(rs);
                list.add(billmaterialdetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
}

    
}


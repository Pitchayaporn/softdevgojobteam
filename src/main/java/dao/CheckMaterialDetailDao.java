/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.CheckMaterialDetail;

/**
 *
 * @author PT
 */
public class CheckMaterialDetailDao implements Dao<CheckMaterialDetail> {
    @Override
    public CheckMaterialDetail get(int id) {
        CheckMaterialDetail checkmaterialdetail = null;
        String sql = "SELECT * FROM CHECK_MATERIAL_DETAILS WHERE checkmaterialdetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                checkmaterialdetail = new CheckMaterialDetail();
                checkmaterialdetail.setId(rs.getInt("checkmaterialdetail_id"));
                checkmaterialdetail.setMatid(rs.getInt("material_id"));
                checkmaterialdetail.setMatname(rs.getString("material_name"));
                checkmaterialdetail.setStatus(rs.getString("status"));
                checkmaterialdetail.setQty(rs.getInt("qty"));
                checkmaterialdetail.setDateexp(rs.getString("date_exp"));
                checkmaterialdetail.setMin(rs.getInt("min"));
                checkmaterialdetail.setUnit(rs.getString("unit"));
                checkmaterialdetail.setLastdate(rs.getDate("last_update"));
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkmaterialdetail;
    }

    @Override
    public List<CheckMaterialDetail> getAll() {
        ArrayList<CheckMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_MATERIAL_DETAILS";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                CheckMaterialDetail checkmaterialdetail = new CheckMaterialDetail();
                checkmaterialdetail.setId(rs.getInt("checkmaterialdetail_id"));
                checkmaterialdetail.setMatid(rs.getInt("material_id"));
                checkmaterialdetail.setMatname(rs.getString("material_name"));
                checkmaterialdetail.setStatus(rs.getString("status"));
                checkmaterialdetail.setQty(rs.getInt("qty"));
                checkmaterialdetail.setDateexp(rs.getString("date_exp"));
                checkmaterialdetail.setMin(rs.getInt("min"));
                checkmaterialdetail.setUnit(rs.getString("unit"));
                checkmaterialdetail.setLastdate(rs.getDate("last_update"));
                
                list.add(checkmaterialdetail);
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    //this use
    public List<CheckMaterialDetail> getAll(String order) {
        ArrayList<CheckMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_MATERIAL_DETAILS ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterialDetail checkmaterialdetail = CheckMaterialDetail.fromRS(rs);
                list.add(checkmaterialdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    //Cant Use Buggy <T-T>
    public List<CheckMaterialDetail> getshow(String order) {
        ArrayList<CheckMaterialDetail> list = new ArrayList();
        String sql = "SELECT material_id, material_name, status, Unit, date_exp, min, qty FROM CHECK_MATERIAL_DETAILS ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterialDetail checkmaterialdetail = CheckMaterialDetail.fromRS(rs);
                list.add(checkmaterialdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override //fine test 
    public CheckMaterialDetail save(CheckMaterialDetail obj) {
        String sql = "INSERT INTO CHECK_MATERIAL_DETAILS (checkmaterialdetail_id, material_id, material_name, status, qty, date_exp, min, unit)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            stmt.setInt(2,obj.getMatid());
            stmt.setString(3,obj.getMatname());
            stmt.setString(4,obj.getStatus());
            stmt.setInt(5,obj.getQty());
            stmt.setString(6,obj.getDateexp());
            stmt.setInt(7,obj.getMin());
            stmt.setString(8,obj.getUnit());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override //fine test 
    public CheckMaterialDetail update(CheckMaterialDetail obj) {
        String sql = "UPDATE CHECK_MATERIAL_DETAILS"
                    + " SET material_id = ?, material_name = ?, status = ?, qty = ?, date_exp = ?, min = ?, unit = ?"
                    + " WHERE checkmaterialdetail_id = ?";
            Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getMatid());
            stmt.setString(2,obj.getMatname());
            stmt.setString(3,obj.getStatus());
            stmt.setInt(4,obj.getQty());
            stmt.setString(5,obj.getDateexp());
            stmt.setInt(6,obj.getMin());
            stmt.setString(7,obj.getUnit());
            stmt.setInt(8,obj.getId());
            
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override //fine test 
    public int delete(CheckMaterialDetail obj) {
        String sql = "DELETE FROM CHECK_MATERIAL_DETAILS WHERE checkmaterialdetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; 
    }

    @Override
    public List<CheckMaterialDetail> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}

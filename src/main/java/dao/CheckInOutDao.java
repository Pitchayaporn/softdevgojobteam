 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import static java.lang.Integer.parseInt;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import model.CheckInOut;

/**
 *
 * @author pasinee
 */
public class CheckInOutDao implements Dao<CheckInOut> {

    @Override
    public CheckInOut get(int id) {
        CheckInOut reciept = null;
        String sql = "SELECT * FROM CHECK_IN_OUT WHERE check_in_out_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                reciept = CheckInOut.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return reciept;
    }
    
    public CheckInOut getLast() {
        CheckInOut reciept = null;
        String sql = "SELECT * FROM CHECK_IN_OUT WHERE check_in_out_id = (SELECT MAX(check_in_out_id) FROM CHECK_IN_OUT)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                reciept = CheckInOut.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return reciept;
    }

    public CheckInOut getByEmpId(int name) {
        CheckInOut reciept = null;
        String sql = "SELECT * FROM CHECK_IN_OUT WHERE emp_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                reciept = CheckInOut.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return reciept;
    }

    public List<CheckInOut> getAll() {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_IN_OUT ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut reciept = CheckInOut.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<CheckInOut> getAll(String where, String order) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_IN_OUT WHERE" + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut reciept = CheckInOut.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<CheckInOut> getAll(String order) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_IN_OUT  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut reciept = CheckInOut.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckInOut save(CheckInOut obj) {

        String sql = "INSERT INTO CHECK_IN_OUT (emp_id) VALUES(?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
    
    public CheckInOut updateStu(CheckInOut obj) {
        String sql = "UPDATE CHECK_IN_OUT"
                + " SET check_status = ?"
                + " WHERE check_in_out_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        String stu;
        String ci = (String.valueOf(obj.getCheckIn()));
        int intci = parseInt(ci.substring(0,2));
        System.out.println(intci);
        if(intci <= 8 && intci >= 6){
            stu = "Intime";
        }else{
            stu = "Late";
        }
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, stu);
            stmt.setInt(2, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public CheckInOut update(CheckInOut obj) {
        String sql = "UPDATE CHECK_IN_OUT"
                + " SET emp_id = ?,check_status = ?"
                + " WHERE check_in_out_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpId());
            stmt.setString(2, obj.getCheckStatus());
            stmt.setInt(3, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public CheckInOut updateLogout(CheckInOut obj) {
        String sql = "UPDATE CHECK_IN_OUT"
                + " SET check_out = (datetime('now', 'localtime')),"
                + " total_hr = ? "
                + " WHERE check_in_out_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            String ci = (String.valueOf(obj.getCheckIn()));
            String co = (String.valueOf(obj.getCheckOut()));
            int intci = parseInt(ci.substring(0,2));
            int intco = parseInt(co.substring(0,2));
            int diff = intco-intci;
            stmt.setInt(1,diff );
            stmt.setInt(2, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(intci);
            System.out.println(intco);
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    

    @Override
    public int delete(CheckInOut obj) {
        String sql = "DELETE FROM CHECK_IN_OUT WHERE check_in_out_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}

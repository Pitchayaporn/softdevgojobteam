/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.CheckMaterial;

/**
 *
 * @author PT
 */
public class CheckMaterialDao implements Dao<CheckMaterial> {

    @Override //SELECT ALL FROM TABLE WHERE checkmaterial_id
    public CheckMaterial get(int id) {
        CheckMaterial checkmaterial = null;
        String sql = "SELECT * FROM CHECK_MATERIAL WHERE checkmaterial_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                checkmaterial = new CheckMaterial();
                checkmaterial.setId(rs.getInt("checkmaterial_id"));
                checkmaterial.setEmpid(rs.getInt("emp_id"));
                checkmaterial.setFname(rs.getString("emp_fname"));
                checkmaterial.setLname(rs.getString("emp_lname"));
                checkmaterial.setDate(rs.getDate("checkmaterial_date"));
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkmaterial;
    }

    @Override //SELECT ALL FROM TABLE
    public List<CheckMaterial> getAll() {
        ArrayList<CheckMaterial> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_MATERIAL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterial checkmaterial = new CheckMaterial();
                checkmaterial.setId(rs.getInt("checkmaterial_id"));
                checkmaterial.setEmpid(rs.getInt("emp_id"));
                checkmaterial.setFname(rs.getString("emp_fname"));
                checkmaterial.setLname(rs.getString("emp_lname"));
                checkmaterial.setDate(rs.getDate("checkmaterial_date"));

                list.add(checkmaterial);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    //SELECT ALL order
    public List<CheckMaterial> getAll(String order) {
        ArrayList<CheckMaterial> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_MATERIAL ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterial checkmaterial = CheckMaterial.fromRS(rs);
                list.add(checkmaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    //SAVE
    @Override
    public CheckMaterial save(CheckMaterial obj) {
        String sql = "INSERT INTO CHECK_MATERIAL (checkmaterial_id, emp_id, emp_fname, emp_lname)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            stmt.setInt(2,obj.getEmpid());
            stmt.setString(3,obj.getFname());
            stmt.setString(4,obj.getLname());
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
    //UPDATE
    @Override
    public CheckMaterial update(CheckMaterial obj) {
        try {
            String sql = "UPDATE CHECK_MATERIAL"
                    + " SET emp_id = ?, emp_fname = ?, emp_lname = ?"
                    + " WHERE checkmaterial_id = ?";
            Connection conn = DatabaseHelper.getConnect();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getEmpid());
            stmt.setString(2,obj.getFname());
            stmt.setString(3,obj.getLname());
            stmt.setInt(4,obj.getId());
            
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    //DELETE
    @Override
    public int delete(CheckMaterial obj) {
        String sql = "DELETE FROM CHECK_MATERIAL WHERE checkmaterial_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<CheckMaterial> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    

}

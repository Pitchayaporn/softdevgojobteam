/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.BillMaterial;

/**
 *
 * @author Paweena Chinasri
 */
public class BillMaterialDao implements Dao<BillMaterial>{

    @Override
    public BillMaterial get(int id) {
        BillMaterial billmaterial = null;
        String sql = "SELECT * FROM BILL_MATERIAL WHERE bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                billmaterial = BillMaterial.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billmaterial;
    }

    @Override
    public List<BillMaterial> getAll() {
        ArrayList<BillMaterial> list = new ArrayList();
        String sql = "SELECT * FROM BILL_MATERIAL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterial billmaterial = BillMaterial.fromRS(rs);
                billmaterial.setBill_id(rs.getInt("bill_id"));
                billmaterial.setEmp_id(rs.getInt("emp_id"));
                billmaterial.setVen_name(rs.getString("ven_name"));
                billmaterial.setBill_total_price(rs.getFloat("bill_total_price"));
                billmaterial.setBill_received(rs.getFloat("bill_received"));
                billmaterial.setBill_change(rs.getFloat("bill_change"));
                billmaterial.setBill_pur_date(rs.getDate("bill_pur_date"));
                billmaterial.setBill_discount(rs.getFloat("bill_discount"));
                billmaterial.setBill_net_price(rs.getFloat("bill_net_price"));
                list.add(billmaterial);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillMaterial save(BillMaterial obj) {
        String sql = "INSERT INTO BILL_MATERIAL (emp_id, ven_name, bill_total_price, bill_received, bill_change, bill_discount, bill_net_price)"
                + "VALUES( ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
         try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmp_id());
            stmt.setString(2, obj.getVen_name());
            stmt.setFloat(3, obj.getBill_total_price());
            stmt.setFloat(4, obj.getBill_received());
            stmt.setFloat(5, obj.getBill_change());
            stmt.setFloat(6, obj.getBill_discount());
            stmt.setFloat(7, obj.getBill_net_price());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setBill_id(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BillMaterial update(BillMaterial obj) {
        String sql = "UPDATE BILL_MATERIAL"
                + " SET emp_id = ?, ven_name = ?, bill_total_price = ?, bill_received = ?, bill_change = ?, bill_discount = ?, bill_net_price = ?"
                + " WHERE bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmp_id());
            stmt.setString(2, obj.getVen_name());
            stmt.setFloat(3, obj.getBill_total_price());
            stmt.setFloat(4, obj.getBill_received());
            stmt.setFloat(5, obj.getBill_discount());
            stmt.setFloat(6, obj.getBill_net_price());
            stmt.setInt(7, obj.getBill_id());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(BillMaterial obj) {
        String sql = "DELETE FROM BILL_MATERIAL WHERE bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBill_id());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    

    @Override
    public List<BillMaterial> getAll(String where, String order) {
        ArrayList<BillMaterial> list = new ArrayList();
        String sql = "SELECT * FROM BILL_MATERIAL where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterial billmaterial = BillMaterial.fromRS(rs);
                list.add(billmaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<BillMaterial> getAll(String order) {
        ArrayList<BillMaterial> list = new ArrayList();
        String sql = "SELECT * FROM BILL_MATERIAL ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillMaterial billmaterial = BillMaterial.fromRS(rs);
                list.add(billmaterial);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
}
}

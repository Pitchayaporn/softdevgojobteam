/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import static java.lang.Integer.parseInt;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.CheckInOut;
import model.Reciept;
import model.RecieptDetail;

/**
 *
 * @author Arthaphan
 */
public class RecieptDao  {

 
    public Reciept get(int id) {
        Reciept reciept = null;
        String sql = "SELECT * FROM RECEIPT WHERE rc_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                reciept = Reciept.fromRS(rs);
                RecieptDetailDao rdd = new RecieptDetailDao();
                ArrayList<RecieptDetail> recieptDetails = (ArrayList<RecieptDetail>) rdd.getAll("reciept_id = "+reciept.getId(), " reciept_detail_id");
                
                reciept.setRecieptDetails(recieptDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return reciept;
    }


    public List<Reciept> getAll() {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
   
    public List<Reciept> getAll(String where, String order) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Reciept> getAll(String order) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

   
    public Reciept save(Reciept obj) {

        String sql = "INSERT INTO RECEIPT (rc_totalprice, rc_cash, rc_totalqty, emp_id, mem_id, pmt_id)"
                + " VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getCash());
            stmt.setInt(3, obj.getTotalQty());
            stmt.setInt(4, obj.getEmpId());
            stmt.setInt(5, obj.getMemId());
            stmt.setInt(6, obj.getPmtId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

  
    public Reciept update(Reciept obj) {
        String sql = "UPDATE RECEIPT"
                + " SET rc_totalprice = ?, rc_cash = ?, rc_totalqty = ?, emp_id = ?, mem_id = ? ,pmt_id = ?"
                + " WHERE rc_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getCash());
            stmt.setInt(3, obj.getTotalQty());
            stmt.setInt(4, obj.getEmpId());
            stmt.setInt(5, obj.getMemId());
            stmt.setInt(6, obj.getPmtId());
             stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    
    public int delete(Reciept obj) {
        String sql = "DELETE FROM RECEIPT WHERE rc_id= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    //update receipt time and date 
    public CheckInOut updateLogout(CheckInOut obj) {
        String sql = "UPDATE CHECK_IN_OUT"
                + " SET check_out = (datetime('now', 'localtime')),"
                + " total_hr = ? "
                + " WHERE check_in_out_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            String ci = (String.valueOf(obj.getCheckIn()));
            String co = (String.valueOf(obj.getCheckOut()));
            int intci = parseInt(ci.substring(0,2));
            int intco = parseInt(co.substring(0,2));
            int diff = intco-intci;
            stmt.setInt(1,diff );
            stmt.setInt(2, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(intci);
            System.out.println(intco);
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
}
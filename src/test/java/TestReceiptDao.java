
import dao.RecieptDao;
import java.util.List;
import java.util.stream.Collectors;
import model.Reciept;
import service.RecieptService;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author kritk
 */
public class TestReceiptDao {
    public static void main(String[] args) {
        RecieptService rs =new RecieptService();
        List<Reciept> receipts = rs.getReciepts();

        // Extracting all IDs using Java 8 streams
        List<Integer> allId = receipts.stream()
                                      .map(Reciept::getId)
                                      .collect(Collectors.toList());

        // Displaying the extracted IDs
        System.out.println("List of IDs: " + allId);
    }
}
